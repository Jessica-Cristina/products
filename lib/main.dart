import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/domain/features/catalog/repositories/mock_backend_repository.dart';
import 'domain/features/catalog/blocs/catalog_bloc.dart';
import 'domain/features/catalog/views/catalog_view.dart';
import 'package:poc_clean/domain/features/cart/repositories/cart_repository.dart';

final cartRepository = CartRepository();
final productRepository = MockBackendResository();

void main() {
  runApp(
    MaterialApp(
      title: 'Products',
      debugShowCheckedModeBanner: false,
      home: BlocProvider<CatalogBloc>(
        create: (context) => CatalogBloc(productRepository),
        child: const CatalogView(),
      ),
    ),
  );
}
