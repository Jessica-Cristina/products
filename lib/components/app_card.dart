import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_bloc.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_bloc.dart';
import 'package:poc_clean/domain/features/catalog/views/detailing_view.dart';

import '../main.dart';

class AppCard extends StatelessWidget {
  const AppCard(
      {Key? key,
      required this.isCart,
      required Product produtos,
      this.onRemove})
      : _produto = produtos,
        super(key: key);

  final Product _produto;
  final bool isCart;
  final VoidCallback? onRemove;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text(
              _produto.name,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                _produto.description,
                textAlign: TextAlign.justify,
              ),
            ),
            Text(
              _produto.valor,
              style: const TextStyle(fontSize: 25),
              textAlign: TextAlign.center,
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return MultiBlocProvider(providers: [
                        BlocProvider(
                          create: (context) => CatalogBloc(productRepository),
                        ),
                        BlocProvider(
                          create: (context) => Cartbloc(),
                        )
                      ], child: DetailingView(id: _produto.id));
                    },
                  ),
                );
              },
              child: const Text('Detalhes'),
            ),
            if (isCart)
              TextButton(
                  onPressed: onRemove,
                  child: const Text(
                    'Remover Produto',
                    style: TextStyle(color: Colors.red),
                  )),
          ],
        ),
      ),
    );
  }
}
