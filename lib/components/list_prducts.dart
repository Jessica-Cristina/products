import 'package:flutter/material.dart';
import 'package:poc_clean/domain/entities/poduct.dart';

import 'app_card.dart';

class ListProducts extends StatelessWidget {
  final List<Product> products;
  const ListProducts({
    required this.products,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: products.length,
      itemBuilder: (context, index) {
        return AppCard(
          produtos: products[index],
          isCart: false,
        );
      },
      separatorBuilder: (context, _) => const Divider(),
    );
  }
}
