import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_bloc.dart';
import 'package:poc_clean/domain/features/cart/views/cart_view.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_bloc.dart';

import '../main.dart';
import '../search_page.dart';

class AppScaffold extends StatelessWidget {
  final Widget child;
  const AppScaffold(this.child, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Products"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => BlocProvider<CatalogBloc>(
                      create: (context) => CatalogBloc(productRepository),
                      child: const SearchPage(),
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.search)),
          IconButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => BlocProvider<Cartbloc>(
                      create: (context) => Cartbloc(),
                      child: const CartView(),
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.shopping_cart_outlined)),
        ],
      ),
      body: SafeArea(
        child: child,
      ),
    );
  }
}
