import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'components/list_prducts.dart';
import 'domain/features/catalog/blocs/catalog_bloc.dart';
import 'domain/features/catalog/blocs/catalog_event.dart';
import 'domain/features/catalog/blocs/catalog_state.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late CatalogBloc _bloc;
  final TextEditingController _searchController = TextEditingController();
  @override
  void initState() {
    super.initState();
    _bloc = context.read<CatalogBloc>();

    _searchController.addListener(() {
      _bloc.add(CatalogSearchEvent(_searchController.text));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          controller: _searchController,
          style: const TextStyle(color: Colors.white),
          decoration: const InputDecoration(
            hintText: 'Search by price here ...',
            hintStyle: TextStyle(color: Colors.white30),
            border: InputBorder.none,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<CatalogBloc, CatalogState>(
          builder: (context, state) {
            if (state is CatalogLoadedState) {
              return ListProducts(products: state.products);
            } else if (state is CatalogInicialState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is CatalogErrorState) {
              return Center(
                child: Text(state.errorMessage),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
