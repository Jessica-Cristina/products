import 'package:poc_clean/domain/entities/poduct.dart';

import '../../main.dart';

class GetCartProducts {
  Stream<List<Product>> call() async* {
    final cartIds = await _getCartProductsIds();
    final products = await _getProducts();
    final list = <Product>[];
    for (final cartId in cartIds) {
      for (final product in products) {
        if (cartId == product.id) list.add(product);
      }
    }
    yield list;
    // yield products.where((product) => cartIds.contains(product.id)).toList();
  }

  Future<List<int>> _getCartProductsIds() async {
    final cartStream = cartRepository.getProducts();
    final ids = await cartStream.first;
    return ids;
  }

  Future<List<Product>> _getProducts() async {
    final productsStream = productRepository.getProducts("");
    final products =
        await productsStream.where((event) => event.isNotEmpty).first;
    return products;
  }
}
