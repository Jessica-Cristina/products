import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/repositories/backend_repository.dart';

class GetProducts {
  String? params;
  final BackendRepositoryInterface respository;
  GetProducts(this.respository, {this.params});

  Stream<List<Product>> call() {
    return respository.getProducts(params);
  }
}
