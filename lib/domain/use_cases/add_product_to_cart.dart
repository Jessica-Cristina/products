import 'package:poc_clean/domain/repositories/cart_repository_interface.dart';

class AddProductToCart {
  final CartRepositoryInterface _cartRepository;

  AddProductToCart(this._cartRepository);

  void call(int id) {
    _cartRepository.addProduct(id);
  }
}
