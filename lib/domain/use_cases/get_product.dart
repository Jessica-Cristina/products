import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/repositories/backend_repository.dart';

class GetProduct {
  final int id;
  final BackendRepositoryInterface repository;
  GetProduct(
    this.id,
    this.repository,
  );

  Stream<Product> call() {
    return repository.getProduct(id);
  }
}
