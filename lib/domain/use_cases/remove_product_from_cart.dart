import 'package:poc_clean/domain/repositories/cart_repository_interface.dart';

class RemoveProductFromCart {
  final CartRepositoryInterface _cartRepository;

  RemoveProductFromCart(this._cartRepository);

  void call(int id) {
    _cartRepository.removeProduct(id);
  }
}
