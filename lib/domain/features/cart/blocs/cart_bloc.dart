// ignore_for_file: invalid_use_of_visible_for_testing_member

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_event.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_state.dart';
import 'package:poc_clean/domain/use_cases/add_product_to_cart.dart';
import 'package:poc_clean/domain/use_cases/get_cart_products.dart';
import 'package:poc_clean/domain/use_cases/remove_product_from_cart.dart';

import '../../../../main.dart';

class Cartbloc extends Bloc<CartEvent, CartState> {
  var list = <Product>[];

  Cartbloc() : super(CartInicialState()) {
    _onGet();
    on<CartAddEvent>(_onAdd);
    on<CartRemoveEvent>(_onRemove);
    on<CartErrorEvent>(_error);
  }

  Future<void> _onAdd(CartAddEvent event, Emitter<CartState> emit) async {
    final addToCart = AddProductToCart(cartRepository);
    addToCart.call(event.id);
    emit(CartLoadedState(list));
  }

  Future<void> _onGet() async {
    final getCart = GetCartProducts();
    list = await getCart.call().where((event) => event.isNotEmpty).first;
    emit(CartLoadedState(list));
  }

  Future<void> _onRemove(CartRemoveEvent event, Emitter<CartState> emit) async {
    final removeFromCart = RemoveProductFromCart(cartRepository);
    removeFromCart.call(event.id);
    emit(CartLoadedState(list));
  }

  Future<void> _error(CartErrorEvent event, Emitter<CartState> emit) async {
    emit(CartErrorState());
  }
}
