import 'package:poc_clean/domain/entities/poduct.dart';

abstract class CartState {}

class CartInicialState extends CartState {}

class CartLoadedState extends CartState {
  final List<Product> products;
  CartLoadedState(this.products);
}

class CartErrorState extends CartState {
  final String errorMessage = 'Lista vazia!';
}
