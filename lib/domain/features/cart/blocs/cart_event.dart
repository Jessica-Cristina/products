import 'package:poc_clean/domain/entities/poduct.dart';

abstract class CartEvent {}

class CartGetEvent extends CartEvent {
  List<Product> products;
  CartGetEvent(this.products);
}

class CartAddEvent extends CartEvent {
  int id;
  CartAddEvent(this.id);
}

class CartRemoveEvent extends CartEvent {
  int id;
  CartRemoveEvent(this.id);
}

class CartErrorEvent extends CartEvent {}
