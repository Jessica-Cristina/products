import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/components/app_card.dart';
import 'package:poc_clean/components/app_scaffold.dart';
import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_bloc.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_event.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_state.dart';

class CartView extends StatefulWidget {
  const CartView({Key? key}) : super(key: key);

  @override
  State<CartView> createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  late Cartbloc _cartBloc;
  late List<Product> list;
  @override
  void initState() {
    super.initState();
    _cartBloc = context.read<Cartbloc>();
    list = _cartBloc.list;
    _cartBloc.add(CartGetEvent(list));
    if (list.isEmpty) _cartBloc.add(CartErrorEvent());
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      SingleChildScrollView(
        child: BlocBuilder<Cartbloc, CartState>(
          builder: (context, state) {
            if (state is CartLoadedState) {
              return ListView.builder(
                shrinkWrap: true,
                itemCount: state.products.length,
                itemBuilder: (context, index) {
                  return AppCard(
                    produtos: state.products[index],
                    isCart: true,
                    onRemove: () => CartRemoveEvent(state.products[index].id),
                  );
                },
              );
            } else if (state is CartInicialState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is CartErrorState) {
              return Center(
                child: Text(
                  state.errorMessage,
                  style: const TextStyle(
                    fontSize: 20,
                    fontStyle: FontStyle.italic,
                  ),
                  textAlign: TextAlign.center,
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
