import 'package:poc_clean/domain/repositories/cart_repository_interface.dart';

class CartRepository implements CartRepositoryInterface {
  final _productsIDs = <int>[];

  @override
  void addProduct(int id) {
    if (_productsIDs.contains(id)) return;
    _productsIDs.add(id);
  }

  @override
  Stream<List<int>> getProducts() async* {
    await Future.delayed(const Duration(seconds: 1));
    yield _productsIDs;
  }

  @override
  void removeProduct(int id) {
    if (!_productsIDs.contains(id)) return;
    _productsIDs.remove(id);
  }
}
