import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/components/app_scaffold.dart';
import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_bloc.dart';
import 'package:poc_clean/domain/features/cart/blocs/cart_event.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_bloc.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_event.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_state.dart';

class DetailingView extends StatefulWidget {
  const DetailingView({required this.id, Key? key}) : super(key: key);
  final int id;

  @override
  _DetailingViewState createState() => _DetailingViewState();
}

class _DetailingViewState extends State<DetailingView> {
  late CatalogBloc _bloc;
  late Cartbloc _cartBloc;

  final TextStyle _headingStyle = const TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );

  CartEvent get _addCart => CartAddEvent(widget.id);

  @override
  void initState() {
    super.initState();
    _bloc = context.read<CatalogBloc>();
    _cartBloc = context.read<Cartbloc>();
    _bloc.add(CatalogItemEvent(widget.id));
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<CatalogBloc, CatalogState>(
          builder: (context, state) {
            if (state is CatalogLoadedItemState) {
              return _detailsProduct(state.products);
            } else if (state is CatalogInicialState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is CatalogErrorState) {
              return Center(
                child: Text(state.errorMessage),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  Column _detailsProduct(Product product) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const SizedBox(height: 24),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              product.name,
              style: _headingStyle,
            ),
            Text(
              product.valor,
              style: _headingStyle,
            ),
          ],
        ),
        const SizedBox(height: 12),
        Text(
          product.description,
          textAlign: TextAlign.justify,
        ),
        const SizedBox(height: 24),
        Center(
          child: Image.network(
            product.image,
            fit: BoxFit.contain,
            errorBuilder: (_, __, ___) {
              return const SizedBox();
            },
          ),
        ),
        const SizedBox(height: 20.0),
        TextButton(
          onPressed: () => _cartBloc.add(_addCart),
          child: const Text('Adicionar ao Carrinho!'),
        ),
      ],
    );
  }
}
