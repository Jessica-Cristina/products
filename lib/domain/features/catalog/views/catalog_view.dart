// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/components/app_scaffold.dart';
import 'package:poc_clean/components/list_prducts.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_bloc.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_event.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_state.dart';
import 'package:provider/src/provider.dart';

class CatalogView extends StatefulWidget {
  const CatalogView({Key? key}) : super(key: key);

  @override
  _CatalogViewState createState() => _CatalogViewState();
}

class _CatalogViewState extends State<CatalogView> {
  late CatalogBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = context.read<CatalogBloc>();
    _bloc.add(CatalogSearchEvent(null));
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<CatalogBloc, CatalogState>(
          builder: (context, state) {
            if (state is CatalogLoadedState) {
              return ListProducts(products: state.products);
            } else if (state is CatalogInicialState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is CatalogErrorState) {
              return Center(
                child: Text(state.errorMessage),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
