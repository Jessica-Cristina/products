import 'package:poc_clean/domain/entities/poduct.dart';

abstract class CatalogState {}

class CatalogInicialState extends CatalogState {
  final List<Product> products;
  CatalogInicialState(this.products);
}

class CatalogLoadingState extends CatalogState {
  final List<Product> products;
  CatalogLoadingState(this.products);
}

class CatalogLoadedState extends CatalogState {
  final List<Product> products;
  CatalogLoadedState(this.products);
}

class CatalogLoadedItemState extends CatalogState {
  final Product products;
  CatalogLoadedItemState(this.products);
}

class CatalogErrorState extends CatalogState {
  final String errorMessage = "Deu Erro aqui!";
}
