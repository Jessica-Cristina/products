import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean/domain/features/catalog/repositories/mock_backend_repository.dart';
import 'package:poc_clean/domain/repositories/backend_repository.dart';
import 'package:poc_clean/domain/use_cases/get_products.dart';
import 'catalog_event.dart';
import 'catalog_state.dart';

class CatalogBloc extends Bloc<CatalogEvent, CatalogState> {
  final BackendRepositoryInterface repository;
  CatalogBloc(this.repository) : super(CatalogInicialState([])) {
    on<CatalogSearchEvent>(_onSearch);
    on<CatalogItemEvent>(_onId);
  }

  Future<void> _onSearch(
    CatalogSearchEvent event,
    Emitter<CatalogState> emit,
  ) async {
    final getProducts = GetProducts(repository, params: event.params);
    final products = await getProducts().where((list) => list.isNotEmpty).first;
    emit(CatalogLoadedState(products));
  }
}

Future<void> _onId(CatalogItemEvent event, Emitter<CatalogState> emit) async {
  final getProducts = GetProducts(MockBackendResository());
  final products = await getProducts().where((list) => list.isNotEmpty).first;
  final product = products.firstWhere((element) => element.id == event.id);
  emit(CatalogLoadedItemState(product));
}
