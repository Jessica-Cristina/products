abstract class CatalogEvent {}

class CatalogSearchEvent extends CatalogEvent {
  String? params;
  CatalogSearchEvent(this.params);
}

class CatalogItemEvent extends CatalogEvent {
  int id;
  CatalogItemEvent(this.id);
}
