import 'dart:async';

import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/repositories/backend_repository.dart';

class MockBackendResository implements BackendRepositoryInterface {
  @override
  Stream<List<Product>> getProducts(String? params) async* {
    await Future.delayed(const Duration(seconds: 2));
    var result = [
      Product(
        id: 1,
        name: 'Produto A',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget mi proin sed libero enim sed faucibus turpis in. Mattis molestie a iaculis at erat pellentesque adipiscing commodo elit. Id venenatis a condimentum vitae sapien pellentesque habitant morbi tristique.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '19,90',
      ),
      Product(
        id: 2,
        name: 'Produto B',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pharetra diam sit amet nisl suscipit. Bibendum enim facilisis gravida neque convallis. Turpis egestas pretium aenean pharetra.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '27,80',
      ),
      Product(
        id: 3,
        name: 'Produto C',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis enim ut tellus elementum sagittis vitae et leo duis. Feugiat sed lectus vestibulum mattis ullamcorper velit sed. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '21,00',
      ),
      Product(
        id: 4,
        name: 'Produto D',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Montes nascetur ridiculus mus mauris. Nisi quis eleifend quam adipiscing. At elementum eu facilisis sed odio.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '0,20',
      ),
      Product(
        id: 5,
        name: 'Produto E',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis rhoncus urna neque viverra justo nec ultrices. In dictum non consectetur a erat nam. Eleifend quam adipiscing vitae proin sagittis nisl.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '8,20',
      ),
      Product(
        id: 6,
        name: 'Produto F',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Libero justo laoreet sit amet cursus sit amet. Placerat in egestas erat imperdiet. Tellus in metus vulputate eu scelerisque.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '9,20',
      ),
      Product(
        id: 7,
        name: 'Produto H',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat pretium nibh ipsum consequat nisl. Tristique senectus et netus et malesuada fames ac turpis. Convallis tellus id interdum velit laoreet id.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '20,90',
      ),
      Product(
        id: 8,
        name: 'Produto I',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt praesent semper feugiat nibh sed pulvinar proin. Risus pretium quam vulputate dignissim suspendisse in est ante in. Mi ipsum faucibus vitae aliquet nec ullamcorper sit.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '22,00',
      ),
      Product(
        id: 9,
        name: 'Produto J',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Gravida in fermentum et sollicitudin ac orci phasellus. Ac turpis egestas sed tempus urna et. Malesuada pellentesque elit eget gravida cum sociis natoque.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '212,00',
      ),
      Product(
        id: 10,
        name: 'Produto K',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae tempus quam pellentesque nec nam. Justo donec enim diam vulputate ut. At tempor commodo ullamcorper a lacus.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '240,89',
      ),
      Product(
        id: 11,
        name: 'Produto L',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Curabitur gravida arcu ac tortor dignissim convallis. Odio facilisis mauris sit amet massa vitae tortor. Viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '220,50',
      ),
      Product(
        id: 12,
        name: 'Produto M',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Imperdiet massa tincidunt nunc pulvinar sapien et ligula. Gravida in fermentum et sollicitudin ac orci phasellus egestas tellus. Nibh venenatis cras sed felis eget velit aliquet.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '120,00',
      ),
    ];
    if (params == null) {
      yield result;
    } else {
      yield result.where((product) => product.valor.contains(params)).toList();
    }
  }

  @override
  Stream<Product> getProduct(int id) async* {
    final _products =
        await getProducts("").where((event) => event.isNotEmpty).first;
    final product = _products.firstWhere((element) => element.id == id);
    yield product;
  }
}
