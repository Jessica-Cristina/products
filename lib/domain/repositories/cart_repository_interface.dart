abstract class CartRepositoryInterface {
  void addProduct(int id);
  void removeProduct(int id);
  Stream<List<int>> getProducts();
}
