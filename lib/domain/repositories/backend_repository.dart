import 'package:poc_clean/domain/entities/poduct.dart';

abstract class BackendRepositoryInterface {
  Stream<List<Product>> getProducts(String? params);
  Stream<Product> getProduct(int id);
}
