import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_bloc.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_event.dart';
import 'package:poc_clean/domain/features/catalog/blocs/catalog_state.dart';
import 'package:poc_clean/domain/features/catalog/repositories/mock_backend_repository.dart';

class MockRepository extends Mock implements MockBackendResository {}

void main() {
  group('Catalog test', () {
    var lista = [
      Product(
        id: 1,
        name: 'Produto A',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget mi proin sed libero enim sed faucibus turpis in. Mattis molestie a iaculis at erat pellentesque adipiscing commodo elit. Id venenatis a condimentum vitae sapien pellentesque habitant morbi tristique.',
        image:
            'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
        valor: '19,90',
      ),
    ];
    final repository = MockRepository();
    blocTest<CatalogBloc, CatalogState>(
      'deve retornar estado [inicial, loaded]',
      setUp: () async => when(() => repository.getProducts(any()))
          .thenAnswer((invocation) => Stream.fromIterable([lista])),
      build: () => CatalogBloc(repository),
      wait: const Duration(seconds: 2),
      act: (bloc) => bloc.add(CatalogSearchEvent("Produto")),
      expect: () => [isA<CatalogLoadedState>()],
    );
  });
}
