import 'package:flutter_test/flutter_test.dart';
import 'package:poc_clean/domain/entities/poduct.dart';
import 'package:poc_clean/domain/features/catalog/repositories/mock_backend_repository.dart';

void main() {
  test('busca produto', () async {
    var product = Product(
      id: 2,
      name: 'Produto B',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pharetra diam sit amet nisl suscipit. Bibendum enim facilisis gravida neque convallis. Turpis egestas pretium aenean pharetra.',
      image:
          'https://static.wixstatic.com/media/2cd43b_b049f322606948bc9143112834f10176~mv2.png/v1/fill/w_810,h_312,fp_0.50_0.50/2cd43b_b049f322606948bc9143112834f10176~mv2.png',
      valor: '27,80',
    );
    final repository = MockBackendResository();
    final result = await repository.getProduct(2).first;
    expect(identical(result.id, product.id), equals(true));
  });

  test('retorna lista de produtos', () async {
    final repository = MockBackendResository();
    final resullt = await repository.getProducts(null).first;
    expect(resullt.isNotEmpty, equals(true));
  });
}
