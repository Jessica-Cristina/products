import 'package:flutter_test/flutter_test.dart';
import 'package:poc_clean/domain/features/cart/repositories/cart_repository.dart';

void main() {
  test('retorna lista de produtos no carrinho', () async {
    final repository = CartRepository();
    final list = await repository.getProducts().first;
    expect(list.isEmpty, equals(true));
  });

  test('deve adicionar produto', () async {
    final repository = CartRepository();
    repository.addProduct(2);
    final list = await repository.getProducts().first;
    expect(list.isNotEmpty, equals(true));
  });
  test('deve remover produto', () async {
    final repository = CartRepository();
    repository.addProduct(2);
    repository.removeProduct(2);
    final list = await repository.getProducts().first;
    expect(list.isEmpty, equals(true));
  });
}
